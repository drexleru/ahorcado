let inter , t;
export const timerLess = function(m, callbackDisplay, callbackFinish){
    t=(m*60000);
    inter=setInterval(function(){
        var minutes = Math.floor(t / 60000);
        var seconds:any = ((t % 60000) / 1000).toFixed(0);
        var display = `${minutes<10?'0'+minutes:minutes}:${seconds<10?'0'+seconds:seconds}`;
        callbackDisplay(display)
        if(t==0){
          clearTimer()
          callbackFinish();
        }
        
        t -= 1000;
    },1000,"JavaScript");

}

export const clearTimer = function(){
    clearInterval(inter);
}