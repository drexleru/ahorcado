const words: string[] = [
    'CARRO',
    'DELFIN',
    'XILOFONO',
    'GALAXIA',
    'AMERICA',
    'FUTBOL',
    'CATORCE'
]

export const wordsCount:number = words.length

export const selectedWord = function():string{
    const min:number = 0;
    const max:number = wordsCount-1;
    const random:number = Math.floor(Math.random() * (max - min + 1)) + min;
    return words[random];
};

export const splitedWord = function():string[]{return selectedWord().split("")};