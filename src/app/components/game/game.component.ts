import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { splitedWord } from '../../data/words';
import { timerLess, clearTimer} from '../../data/timer';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  name: string = '';
  partsHuman: boolean[] = [];
  wordsNoFill: string[] = [];
  lettersAlreadySaid: string[] = [];
  mySplitedWord: string[] = [];
  tryLetterGroup = new FormGroup({
    letter: new FormControl(''),
  })
  nAttempts: number = 0;
  nOk: number = 0;
  time: string = '';
  constructor(private router: Router) { }

  ngOnInit() {
    this.name = localStorage.getItem('name');
    this.setGame();
  }

  private setGame() {
    this.mySplitedWord = splitedWord();   
    for(let i=0; i<8; i++){
      this.partsHuman.push(false)
    }
    for(let i=0; i<this.mySplitedWord.length; i++){
      this.wordsNoFill.push("_")
    }

    timerLess(5, (text) => {
      this.time = text;
    }, () => {
      if(this.nAttempts < 8){
        swal({allowOutsideClick: false, type:'error', title: 'Oopss', html: `
        <h1>Se acabo el tiempo, Perdiste</h1>
        <p>La palabra era ${this.mySplitedWord.join("")}</p>
      `}).then(result => {
        if(result.value){
          this.resetGame();
        }
      });
      }
    })

  }

  tryLetter(){
    const iLetter = this.tryLetterGroup.value.letter.toUpperCase();
    this.tryLetterGroup.reset();
    const existLetter = this.mySplitedWord.includes(iLetter);
    if(existLetter){
      this.mySplitedWord.forEach((item, index) => {
        if(item == iLetter){
          this.wordsNoFill[index] = iLetter;
          this.nOk += 1;
        }
      })
    }else{
      this.partsHuman[this.nAttempts] = true;
      this.lettersAlreadySaid.push(iLetter);
      this.nAttempts += 1;
      
    }
    if(this.nAttempts == 8){
      swal({allowOutsideClick: false, type:'error', title: 'Oopss', html: `
      <h1>Ahorcado, Perdiste</h1>
      <p>La palabra era ${this.mySplitedWord.join("")}</p>
    `}).then(result => {
      if(result.value){
        this.resetGame();
      }
    });
      this.wordsNoFill = this.mySplitedWord;
    }
    
    if(this.nOk == this.mySplitedWord.length){
      swal({allowOutsideClick: false, type:'success', title: 'Bien!!', html: `
        <h1>Ganaste</h1>
        <p>La palabra era ${this.mySplitedWord.join("")}</p>
      `}).then(result => {
        if(result.value){
          this.resetGame();
        }
      });
    }
  }

  resetGame(){
    this.partsHuman = [];
    this.wordsNoFill = [];
    this.lettersAlreadySaid = [];
    this.nAttempts = 0;
    this.nOk = 0;
    clearTimer();
    this.setGame();
  }


  logout(){
    localStorage.removeItem('name');
    localStorage.removeItem('email');
    this.router.navigate(['']);
  }

}
