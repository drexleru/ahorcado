import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  signInForm = new FormGroup({
    name: new FormControl(''),
    email: new FormControl(''),
  })

  incompleteData: boolean = false;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  signIn(){
    const {name, email} = this.signInForm.value;
    if(name.trim().length == 0 && email.trim().length == 0){
      this.incompleteData = true;
    }else{
      this.incompleteData = false;
      localStorage.setItem('name', name);
      localStorage.setItem('email', email);
      this.router.navigate(['/game']);
    }
  }
}
