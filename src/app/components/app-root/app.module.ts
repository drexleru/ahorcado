import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ReactiveFormsModule  } from '@angular/forms';
import { AppComponent } from './app.component';
import {SignInComponent} from '../sign-in/sign-in.component'
import { appRouting } from '../../router';
import { GameComponent } from '../game/game.component';
@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    GameComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    NgbModule,
    SweetAlert2Module.forRoot(),
    appRouting
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
