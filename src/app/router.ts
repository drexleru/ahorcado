import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './components/sign-in/sign-in.component'
import { GameComponent } from './components/game/game.component'
const appRoutes: Routes = [
    {path: '', component: SignInComponent},
    {path: 'game', component: GameComponent}
    //{ path: '**', component: PageNotFoundComponent }
];

export const appRouting = RouterModule.forRoot(appRoutes,{ enableTracing: true })
